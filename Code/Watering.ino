/* Innovativetom.com
  Flower Pot Soil Mosture Sensor
  A0 - Soil Mosture Sensor
  D2:D6 - LEDS 1,2,3,4,5
  LED1 - Green
  LED2 - Green
  LED3 - Green
  LED4 - YELLOW
  LED5 - RED
  Connect the Soil Mosture Sensor to anolog input pin 0,
  and your 5 led to digital out 2-6
*/
#include <EEPROM.h> //Needed to access the eeprom read write functions

int led1 = 2;
int led2 = 3;
int led3 = 4;
int led4 = 5;
int led5 = 6;

int mostureSensor = 0;

// Used to store the lates writen position in EEPROM
int currentPosition = 0;

// Storage variables
//Here we store the index of the EEPROM memory byte that holds the latest writen memory position

void setup() {
  // Serial Begin so we can see the data from the mosture sensor in our serial input window.
  Serial.begin(9600);
  // setting the led pins to outputs
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(led5, OUTPUT);

  //Load the the currentPosition value.
  // Yes, the first ran will start from who knows where, but I can leave with that
  int currentPosition = EEPROMReadInt(0);

  sendStoredValues();
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(mostureSensor);

  // print out the value you read:
  Serial.println(sensorValue);

  //Save it in case there is no Serial connected
  saveSensorValue(sensorValue);

  if (sensorValue >= 820)
  {
    digitalWrite(led1, HIGH);
    digitalWrite(led2, HIGH);
    digitalWrite(led3, HIGH);
    digitalWrite(led4, HIGH);
    digitalWrite(led5, LOW);
  }
  else if (sensorValue >= 615  && sensorValue < 820)
  {
    digitalWrite(led1, HIGH);
    digitalWrite(led2, HIGH);
    digitalWrite(led3, HIGH);
    digitalWrite(led4, LOW);
    digitalWrite(led5, LOW);
  }
  else if (sensorValue >= 410 && sensorValue < 615)
  {
    digitalWrite(led1, HIGH);
    digitalWrite(led2, HIGH);
    digitalWrite(led3, LOW);
    digitalWrite(led4, LOW);
    digitalWrite(led5, LOW);
  }
  else if (sensorValue >= 250 && sensorValue < 410)
  {
    digitalWrite(led1, HIGH);
    digitalWrite(led2, LOW);
    digitalWrite(led3, LOW);
    digitalWrite(led4, LOW);
    digitalWrite(led5, LOW);
  }
  else if (sensorValue >= 0 && sensorValue < 250)
  {
    digitalWrite(led1, LOW);
    digitalWrite(led2, LOW);
    digitalWrite(led3, LOW);
    digitalWrite(led4, LOW);
    digitalWrite(led5, LOW);
  }
  delay(1000);        // delay 1 second between reads
}

void saveSensorValue(int value) {

  EEPROM.write(currentPosition, value / 4);

  currentPosition++;

  if (currentPosition == EEPROM.length()) {
    // Reset the currentPosition, but be sure that we leave at the beginning of
    // the EEPROM enough space for the "currentPosition" variable
    currentPosition = sizeof(currentPosition);
  }

  // Save the currentPosition, so when reset, we keep writing from where we left
  EEPROMWriteInt(0, currentPosition);
}

void sendStoredValues() {
  if (Serial) {

    // First send from the currentPosition up to the end of the available memory
    for (int i = currentPosition; i < EEPROM.length(); i++) {
      byte value = EEPROM.read(i);

      if (value == 255) {
        // According to the Arduino documentation: Locations that have never been written to have the value of 255.
        // So, I'll asume there are no more writen values (I'll never store a 255, of course...)
        return;
      }

      Serial.println(value * 4);
    }

    // Now send the values from the start, to the currentPosition
    // we dont want to send the "currentPosition" variable value so we start after its storage place
    for (int i = sizeof(currentPosition); i < currentPosition; i++) {
      byte value = EEPROM.read(i);

      if (value == 255) {
        // According to the Arduino documentation: Locations that have never been written to have the value of 255.
        // So, I'll asume there are no more writen values (I'll never store a 255, of course...)
        return;
      }

      Serial.println(value * 4);
    }
  }
}

//This function will write a 2 byte integer to the eeprom at the specified address and address + 1
void EEPROMWriteInt(int p_address, int p_value)
{
  byte lowByte = ((p_value >> 0) & 0xFF);
  byte highByte = ((p_value >> 8) & 0xFF);

  EEPROM.write(p_address, lowByte);
  EEPROM.write(p_address + 1, highByte);
}

//This function will read a 2 byte integer from the eeprom at the specified address and address + 1
unsigned int EEPROMReadInt(int p_address)
{
  byte lowByte = EEPROM.read(p_address);
  byte highByte = EEPROM.read(p_address + 1);

  return ((lowByte << 0) & 0xFF) + ((highByte << 8) & 0xFF00);
}
